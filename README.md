Bot collection
==============

## General setup

Create a virtualenv and install the dependencies into it:

    virtualenv venv
    . venv/bin/activate
    pip install -r requirements.txt

Create a Discord integration (https://discord.com/developers/applications/)

On the "Bot" tab, add a bot. Copy the token into the applicable bot config
(often a file `token.txt`).  (Don't confuse the token with the application ID
on the "general information" tab, or the client ID or secret on the OAuth2 tab
-- you should be on the "Bot" tab.) Depending on the bot, enable "message
content intent" -- this is a privileged intent, so if you want to use the bot
with more than 100 guilds, you'll need to get your bot reviewed. For typical
uses with only a handful of guilds, though, no review is needed.

On the OAuth2 URL generator page, give it the `bot` scope with the applicable
permissions.

This will produce a URL with these permissions and some client ID, along the lines of:
https://discord.com/api/oauth2/authorize?client_id=1150279244539248651&permissions=3136&scope=bot

Following this link will allow adding the integration to a Discord server you have Manager Server permissions on.

## RemindBot

Permissions for RemindBot:
- Read messages
- Send messages
- Add reactions

Sample link: https://discord.com/api/oauth2/authorize?client_id=1150279244539248651&permissions=3136&scope=bot
