#!/usr/bin/env python3

import argparse
import asyncio
import collections
import datetime
import logging
import pprint
import yaml
from zoneinfo import ZoneInfo

import discord

LOGGER = logging.getLogger(__name__)

class RemindClient(discord.Client):
    def __init__(self, config, period):
        intents = discord.Intents(guilds=True, messages=True, message_content=True,
                                  members=True) # Needed for react_only_owner
        super().__init__(intents=intents)
        self.config = config
        self.period = period
        self.periodic_task = None
        self._dup_hist = collections.defaultdict(set)


    async def on_ready(self):
        LOGGER.info(f'Logged on as {self.user}!')
        if not self.periodic_task:
            self.periodic_task = asyncio.create_task(self.periodic())
            # Arguably should unset the var when the task finishes, but it
            # shouldn't finish so I'm lazy
            # https://docs.python.org/3/library/asyncio-task.html#asyncio.create_task


    async def add_reacts_to_message(self, message, words):
        for text, react in words.items():
            if text in message.content.lower():
                LOGGER.info(f"  - adding react {react}")
                await message.add_reaction(react)
                LOGGER.info(f"  - added react {react}")


    def is_owner(self, member):
        if not member.guild.owner:
            LOGGER.warning("Couldn't identify owner, assuming not")
        return member == member.guild.owner


    async def on_message(self, message):
        LOGGER.info(f'Message in {message.guild.name} {message.channel.id} from {message.author}: {message.content}')
        label = self.config['locations'].get(message.guild.id, {}).get(message.channel.id)
        if label:
            LOGGER.info(f"  - matched {label=}")
            only_owner = self.config['reminders'][label]['react_only_owner']
            if only_owner and not self.is_owner(message.author):
                LOGGER.info(f"  - {message.author=} != {message.guild.owner=}, skipping")
                return
            words = self.config['reminders'][label]['words']
            await self.add_reacts_to_message(message, words)
            LOGGER.info(f"  - processed {label=}")


    def check_dup_message(self, channel, msg, priors):
        """Checks if we should skip sending a message for duplicates"""
        first_prior = priors[0].created_at if priors else None
        key = (first_prior, msg)
        if key in self._dup_hist[channel.id]:
            LOGGER.info("      - Backing off message: %s %s", msg, priors)
            return True
        self._dup_hist[channel.id].add(key)
        return False

    async def periodic_rolling(self, channel, reminders):
        rolling = {rem['name']: rem['needed'] for rem in reminders['rolling']}
        allow_dup = reminders['allow_dup']
        include_priors = reminders['include_priors']
        hour = datetime.timedelta(hours=1)
        now = datetime.datetime.now(datetime.timezone.utc)
        max_age = max(rem['interval'] for rem in reminders['rolling'])
        after = now - max_age*hour
        LOGGER.info(f"    - pulling messages {after=}")
        priors = collections.defaultdict(list) # emoji -> list of messages
        async for message in channel.history(after=after, oldest_first=False):
            for rem in reminders['rolling']:
                if message.created_at + rem['interval']*hour < now:
                    continue # too old to count
                for react in message.reactions:
                    points = rem['points'].get(react.emoji, 0)
                    rolling[rem['name']] -= points
                    for i in range(points):
                        priors[rem['name']].append(message)
        LOGGER.debug("     - points needed: %s", rolling)
        for label, points in rolling.items():
            if points > 0:
                msg = f"You should do {points} more {label}"
                prior_times = [str(prior.created_at) for prior in priors[label]]
                if include_priors and prior_times:
                    msg += f' (priors: {", ".join(prior_times)})'
                if allow_dup or not self.check_dup_message(channel, msg, priors[label]):
                    await channel.send(msg)


    async def periodic_schedule(self, channel, reminders):
        # Scheduled events should take place at the scheduled time plus or
        # minus `width`. At scheduled time plus `width` +/- `tolerance`, we
        # check and send messages about anything that's off.
        width = datetime.timedelta(hours=1)
        tolerance = datetime.timedelta(seconds=self.period * 7 / 12)
        now = datetime.datetime.now(ZoneInfo('America/New_York'))
        check_center = now - width
        check_start = (check_center - tolerance).time()
        check_end = (check_center + tolerance).time()
        check_day = check_center.strftime('%A')
        LOGGER.info(f'    - checking {check_start=} {check_end=}')
        for day, time, label, emoji in reminders['schedule']:
            target_time = datetime.time.fromisoformat(time)
            if check_start > check_end:
                # Crossing midnight
                # TODO: Handle this
                # Fortunately, no scheduled slots are less than an hour from
                # midnight, so as long as the period is significantly less
                # than an hour we're fine.
                continue
            else:
                if check_day != day:
                    continue
                if target_time < check_start or target_time > check_end:
                    continue
                target_dt = datetime.datetime.combine(check_center.date(), target_time)

            # Great, this period is relevant
            LOGGER.info(f'    - checking {day=} {time=} {label=} {emoji=}')
            after = target_dt - width
            before = target_dt + width
            before = now
            found = False
            async for message in channel.history(after=after, before=before):
                # Hypothetically this would be kinda a waste because we fetch
                # history a bunch of times, but in practice we only have one
                # scheduled thing at a time, so we probably can't usefully
                # optimize
                for react in message.reactions:
                    if react.emoji == emoji:
                        LOGGER.info(f'      - found {message} {message.content}')
                        found = True
                        break
                if found: break
            if not found:
                LOGGER.info(f'      - not found, scanned all messages {after=} {before=}')
                msg = f"You should do {label}"
                await channel.send(msg)


    async def periodic_channel(self, channel, reminders):
        LOGGER.info(f"  - periodic processing for {channel=}")
        await self.periodic_rolling(channel, reminders)
        await self.periodic_schedule(channel, reminders)


    async def periodic(self):
        while True:
            # Look over past messages
            LOGGER.info("Starting periodic processing")
            for guild, channels in self.config['locations'].items():
                for channel, label in channels.items():
                    channel_obj = self.get_channel(channel)
                    if channel_obj:
                        reminders = self.config['reminders'][label]
                        await self.periodic_channel(channel_obj, reminders)
                    else:
                        LOGGER.warning(f"unknown channel {channel=}")
            # Sleep
            LOGGER.info("Finished periodic processing, sleeping")
            await asyncio.sleep(self.period)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--period', type=int, default=60*30,
                        help='period (in seconds) between loops')
    parser.add_argument('--config', type=str, default='config.yaml')
    parser.add_argument('--token-file', type=str, default='token.txt')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    with open(args.config, 'r') as fp:
        config = yaml.safe_load(fp)
    LOGGER.debug("config %s", pprint.pformat(config))
    client = RemindClient(config, args.period)
    with open(args.token_file, 'r') as token_fp:
        token = token_fp.read().strip()
    client.run(token)
    LOGGER.info("run finished")


if __name__ == '__main__':
    logging.basicConfig()
    LOGGER.setLevel(logging.DEBUG)
    main()
